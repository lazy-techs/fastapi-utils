from datetime import datetime, timezone

from pydantic import BaseModel

from fastapi_utils.pydantic_fields import ISODateTime, TruncatedFloat


def test_iso_datetime_field():
    class MyModel(BaseModel):
        dt: ISODateTime

    model = MyModel(dt=datetime(2023, 12, 10, 10, 10, 10, tzinfo=timezone.utc))
    assert model.model_dump_json() == '{"dt":"2023-12-10T10:10:10+00:00"}'


def test_truncated_float_field():
    class MyModel(BaseModel):
        value: TruncatedFloat

    model = MyModel(value=3.14143472384719)
    assert model.value == 3.14
    assert model.model_dump_json() == '{"value":3.14}'
