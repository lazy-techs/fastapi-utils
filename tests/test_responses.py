from fastapi import FastAPI
from starlette.testclient import TestClient

from fastapi_utils.exception import BadRequestException
from fastapi_utils.responses import get_error_responses


class TestException(BadRequestException):
    message = 'Test exception'


def test_app_route_error_responses():
    app = FastAPI()

    @app.get('/', responses=get_error_responses(TestException))
    async def test_api():
        raise TestException

    with TestClient(app):
        """Nothing"""
