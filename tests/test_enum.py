from pydantic import BaseModel
from typing_extensions import Annotated

from fastapi_utils.enum import BaseEnum, EnumOutSerializer


class MyEnum(BaseEnum):
    first = 1
    second = 2


class MyOutSchema(BaseModel):
    value: Annotated[MyEnum, EnumOutSerializer]


def test_enum__get_by_name():
    assert MyEnum.first == MyEnum('first')


def test_enum__get_by_value():
    assert MyEnum.second == MyEnum(2)


def test_out_schema__enum():
    assert MyOutSchema(value=MyEnum.first).model_dump_json() == '{"value":"first"}'
