from enum import StrEnum

from starlette.requests import Request


class LanguageEnum(StrEnum):
    ru = 'ru'
    en = 'en'


def accept_language(request: Request) -> LanguageEnum:
    language = request.headers.get('Accept-Language')
    if not language:
        return LanguageEnum.en
    if language[:2] == 'ru':
        return LanguageEnum.ru
    if language[:2] == 'en':
        return LanguageEnum.en
    return LanguageEnum.en
