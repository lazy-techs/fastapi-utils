from typing import Any, Awaitable, Callable, Mapping

from aiohttp import (
    BasicAuth,
    ClientResponse,
    ClientSession,
    ClientTimeout,
    ContentTypeError,
    FormData,
    RequestInfo,
)
from aiohttp.typedefs import Query

UrlParams = dict[str, str | int]


class HttpClientResponseError(Exception):
    def __init__(self, request_info: RequestInfo, status_code: int, response_data: Any):
        self.request_info = request_info
        self.status_code = status_code
        self.response_data = response_data

    def __str__(self) -> str:
        return 'url={0!r}, status={1}, response={2!r}'.format(
            self.request_info.url,
            self.status_code,
            self.response_data,
        )


async def json_response_callback(response: ClientResponse) -> Any:
    return await response.json()


async def text_response_callback(response: ClientResponse) -> str:
    return await response.text()


async def raise_for_status_default(response: ClientResponse) -> None:
    if not response.ok:
        try:
            response_data = await response.json()
        except ContentTypeError:
            response_data = await response.text()
        raise HttpClientResponseError(
            request_info=response.request_info,
            status_code=response.status,
            response_data=response_data,
        )


class HttpClient:
    def __init__(
        self,
        host: str,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> None:
        self._host: str = host
        self._headers: Mapping[str, str] | None = headers
        self._auth: BasicAuth | None = auth
        self._timeout: ClientTimeout = timeout if timeout else ClientTimeout(total=10)
        self._response_callback = response_callback

    async def options(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] | None = None,
    ) -> Any:  # noqa: WPS211
        return await self._request(
            'OPTIONS',
            url=url.format(**url_params or {}),
            params=params,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def head(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> Any:
        return await self._request(
            'HEAD',
            url=url.format(**url_params or {}),
            params=params,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def get(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> Any:
        return await self._request(
            'GET',
            url=url.format(**url_params or {}),
            params=params,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def delete(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> Any:
        return await self._request(
            'DELETE',
            url=url.format(**url_params or {}),
            params=params,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def post(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        data: Any | FormData | None = None,
        json: Any | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> Any:
        return await self._request(
            'POST',
            url=url.format(**url_params or {}),
            params=params,
            data=data,
            json=json,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def patch(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        data: Any | FormData | None = None,
        json: Any | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> Any:
        return await self._request(
            'PATCH',
            url=url.format(**url_params or {}),
            params=params,
            data=data,
            json=json,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def put(
        self,
        url: str,
        url_params: UrlParams | None = None,
        params: Query | None = None,
        data: Any | FormData | None = None,
        json: Any | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] = json_response_callback,
    ) -> Any:
        return await self._request(
            'PUT',
            url=url.format(**url_params or {}),
            params=params,
            data=data,
            json=json,
            headers=headers,
            auth=auth,
            timeout=timeout,
            raise_for_status=raise_for_status,
            response_callback=response_callback,
        )

    async def _request(
        self,
        method: str,
        url: str,
        params: Query | None = None,
        data: Any | None = None,
        json: Any | None = None,
        headers: Mapping[str, str] | None = None,
        auth: BasicAuth | None = None,
        timeout: ClientTimeout | None = None,
        raise_for_status: bool | Callable[[ClientResponse], Awaitable[None]] | None = None,
        response_callback: Callable[[ClientResponse], Awaitable[Any]] | None = None,
    ) -> Any:
        async with ClientSession(base_url=self._host) as session:
            async with session.request(
                method=method,
                url=url,
                params=params,
                data=data,
                json=json,
                headers={**(self._headers or {}), **(headers or {})},
                auth=auth or self._auth,
                timeout=timeout or self._timeout,
                raise_for_status=raise_for_status or raise_for_status_default,
                allow_redirects=False,
            ) as response:
                if response_callback:
                    return await response_callback(response)
                return await self._response_callback(response)
