from enum import Enum

from pydantic import PlainSerializer

EnumOutSerializer = PlainSerializer(lambda e: e.name, return_type='str', when_used='always')  # noqa: WPS111


class BaseEnum(Enum):
    @classmethod
    def _missing_(cls, value: str) -> Enum | None:  # type: ignore[override] # noqa: WPS120
        try:
            return cls._member_map_[value]
        except KeyError:
            return None
