from datetime import datetime

from pydantic import AfterValidator, PlainSerializer, WithJsonSchema
from typing_extensions import Annotated

ISODateTime = Annotated[
    datetime,
    PlainSerializer(lambda value: value.isoformat(), return_type=str, when_used='json'),
]  # noqa: WPS221

TruncatedFloat = Annotated[
    float,
    AfterValidator(lambda value: round(value, 2)),
    PlainSerializer(lambda value: float('{0:.2f}'.format(value)), return_type=float),
    WithJsonSchema({'type': 'float'}, mode='serialization'),
]  # noqa: WPS221
