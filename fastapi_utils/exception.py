from fastapi import HTTPException


class BadRequestException(HTTPException):
    message = 'bad_request'
    status_code = 400

    def __init__(self) -> None:
        super().__init__(status_code=self.status_code, detail=self.message)
