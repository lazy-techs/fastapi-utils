from collections import defaultdict
from enum import StrEnum
from typing import Any, Type

from pydantic import create_model

from fastapi_utils.exception import BadRequestException


def get_error_responses(*exceptions: Type[BadRequestException]) -> dict[int | str, dict[str, Any]]:
    responses: dict[int | str, dict[str, Any]] = {}

    errors_by_status_code = defaultdict(list)
    for error in exceptions:
        errors_by_status_code[error.status_code].append(error.message)

    for status_code, messages in errors_by_status_code.items():
        responses[status_code] = {
            'model': create_model(
                'Error{0}'.format(status_code),
                detail=(StrEnum('ErrorEnum', {message: message for message in messages}), ...),
            ),
        }

    return responses
