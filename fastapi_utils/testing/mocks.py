from typing import Generator
from unittest.mock import AsyncMock, patch

import pytest


@pytest.fixture
def http_client_options_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.options') as mock:
        yield mock


@pytest.fixture
def http_client_head_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.head') as mock:
        yield mock


@pytest.fixture
def http_client_get_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.get') as mock:
        yield mock


@pytest.fixture
def http_client_post_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.post') as mock:
        yield mock


@pytest.fixture
def http_client_patch_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.patch') as mock:
        yield mock


@pytest.fixture
def http_client_put_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.put') as mock:
        yield mock


@pytest.fixture
def http_client_delete_mock() -> Generator[AsyncMock, None, None]:
    with patch('fastapi_utils.http_client.HttpClient.delete') as mock:
        yield mock
