from fastapi_utils.http_client import HttpClient


class ExternalClient:
    def __init__(self, host: str):
        self._http_client = HttpClient(host=host)
